TARGET = nfc
TEMPLATE = lib
CONFIG += staticlib
CONFIG -= qt

QMAKE_CFLAGS += -std=gnu99

INCLUDEPATH += ../include
INCLUDEPATH += /usr/include/PCSC

DEFINES += HAVE_CONFIG_H
DEFINES += LOG

SOURCES = \
    conf.cpp \
    iso14443-subr.cpp \
    mirror-subr.cpp \
    nfc.cpp \
    nfc-device.cpp \
    nfc-emulation.cpp \
    nfc-internal.cpp \
    target-subr.cpp \
    log.cpp \
    log-internal.cpp \
    buses/usbbus.cpp \
    chips/pn53x.cpp \
    drivers/acr122_usb.cpp \
    drivers/pn53x_usb.cpp \
    drivers/acr122_pcsc.cpp

HEADERS += \
    conf.h \
    drivers.h \
    iso7816.h \
    log.h \
    log-internal.h \
    mirror-subr.h \
    nfc-internal.h \
    target-subr.h \
    config.h \
    buses/usbbus.h \
    chips/pn53x.h \
    chips/pn53x-internal.h \
    drivers/pn53x_usb.h \
    drivers/acr122_usb.h \
    drivers/acr122_pcsc.h \
    ../include/nfc.h \
    ../include/nfc-emulation.h \
    ../include/nfc-types.h

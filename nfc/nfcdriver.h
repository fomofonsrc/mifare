#ifndef NFCDRIVER_H
#define NFCDRIVER_H

#include <stddef.h>
#include "nfc-types.h"

typedef enum {
	NOT_INTRUSIVE,
	INTRUSIVE,
	NOT_AVAILABLE,
} scan_type_enum;

/**
 * NFC device driver
 */

struct NfcDriver
{
	const char *name;
	const scan_type_enum scan_type;
	size_t (*scan)(const NfcContext *context, nfc_connstring connstrings[], const size_t connstrings_len);
	struct NfcDevice *(*open)(const NfcContext *context, const nfc_connstring connstring);
	void (*close)(struct NfcDevice *pnd);
	const char *(*strerror)(const struct NfcDevice *pnd);

	int (*initiator_init)(struct NfcDevice *pnd);
	int (*initiator_init_secure_element)(struct NfcDevice *pnd);
	int (*initiator_select_passive_target)(struct NfcDevice *pnd,  const nfc_modulation nm, const uint8_t *pbtInitData, const size_t szInitData, NfcTarget *pnt);
	int (*initiator_poll_target)(struct NfcDevice *pnd, const nfc_modulation *pnmModulations, const size_t szModulations, const uint8_t uiPollNr, const uint8_t btPeriod, NfcTarget *pnt);
	int (*initiator_select_dep_target)(struct NfcDevice *pnd, const nfc_dep_mode ndm, const nfc_baud_rate nbr, const nfc_dep_info *pndiInitiator, NfcTarget *pnt, const int timeout);
	int (*initiator_deselect_target)(struct NfcDevice *pnd);
	int (*initiator_transceive_bytes)(struct NfcDevice *pnd, const uint8_t *pbtTx, const size_t szTx, uint8_t *pbtRx, const size_t szRx, int timeout);
	int (*initiator_transceive_bits)(struct NfcDevice *pnd, const uint8_t *pbtTx, const size_t szTxBits, const uint8_t *pbtTxPar, uint8_t *pbtRx, uint8_t *pbtRxPar);
	int (*initiator_transceive_bytes_timed)(struct NfcDevice *pnd, const uint8_t *pbtTx, const size_t szTx, uint8_t *pbtRx, const size_t szRx, uint32_t *cycles);
	int (*initiator_transceive_bits_timed)(struct NfcDevice *pnd, const uint8_t *pbtTx, const size_t szTxBits, const uint8_t *pbtTxPar, uint8_t *pbtRx, uint8_t *pbtRxPar, uint32_t *cycles);
	int (*initiator_target_is_present)(struct NfcDevice *pnd, const NfcTarget *pnt);

	int (*target_init)(struct NfcDevice *pnd, NfcTarget *pnt, uint8_t *pbtRx, const size_t szRx, int timeout);
	int (*target_send_bytes)(struct NfcDevice *pnd, const uint8_t *pbtTx, const size_t szTx, int timeout);
	int (*target_receive_bytes)(struct NfcDevice *pnd, uint8_t *pbtRx, const size_t szRxLen, int timeout);
	int (*target_send_bits)(struct NfcDevice *pnd, const uint8_t *pbtTx, const size_t szTxBits, const uint8_t *pbtTxPar);
	int (*target_receive_bits)(struct NfcDevice *pnd, uint8_t *pbtRx, const size_t szRxLen, uint8_t *pbtRxPar);

	int (*device_set_property_bool)(struct NfcDevice *pnd, const nfc_property property, const bool bEnable);
	int (*device_set_property_int)(struct NfcDevice *pnd, const nfc_property property, const int value);
	int (*get_supported_modulation)(struct NfcDevice *pnd, const nfc_mode mode, const nfc_modulation_type **const supported_mt);
	int (*get_supported_baud_rate)(struct NfcDevice *pnd, const nfc_mode mode, const nfc_modulation_type nmt, const nfc_baud_rate **const supported_br);
	int (*device_get_information_about)(struct NfcDevice *pnd, char **buf);

	int (*abort_command)(struct NfcDevice *pnd);
	int (*idle)(struct NfcDevice *pnd);
	int (*powerdown)(struct NfcDevice *pnd);
};

#endif // NFCDRIVER_H

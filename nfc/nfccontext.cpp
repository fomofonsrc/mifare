#include "nfccontext.h"

NfcContext::NfcContext()
{
	// Set default context values
	allow_autoscan = true;
	allow_intrusive_scan = false;
#ifdef DEBUG
	log_level = 3;
#else
	log_level = 1;
#endif

	// Clear user defined devices array
	for (int i = 0; i < MAX_USER_DEFINED_DEVICES; i++) {
		strcpy(user_defined_devices[i].name, "");
		strcpy(user_defined_devices[i].connstring, "");
		user_defined_devices[i].optional = false;
	}
	user_defined_device_count = 0;

#ifdef ENVVARS
	// Load user defined device from environment variable at first
	char *envvar = getenv("LIBNFC_DEFAULT_DEVICE");
	if (envvar) {
		strcpy(user_defined_devices[0].name, "user defined default device");
		strncpy(user_defined_devices[0].connstring, envvar, NFC_BUFSIZE_CONNSTRING);
		user_defined_devices[0].connstring[NFC_BUFSIZE_CONNSTRING - 1] = '\0';
		user_defined_device_count++;
	}

#endif // ENVVARS

#ifdef CONFFILES
	// Load options from configuration file (ie. /etc/nfc/libnfc.conf)
	conf_load(res);
#endif // CONFFILES

#ifdef ENVVARS
	// Environment variables

	// Load user defined device from environment variable as the only reader
	envvar = getenv("LIBNFC_DEVICE");
	if (envvar) {
		strcpy(user_defined_devices[0].name, "user defined device");
		strncpy(user_defined_devices[0].connstring, envvar, NFC_BUFSIZE_CONNSTRING);
		user_defined_devices[0].connstring[NFC_BUFSIZE_CONNSTRING - 1] = '\0';
		user_defined_device_count = 1;
	}

	// Load "auto scan" option
	envvar = getenv("LIBNFC_AUTO_SCAN");
	string_as_boolean(envvar, &(allow_autoscan));

	// Load "intrusive scan" option
	envvar = getenv("LIBNFC_INTRUSIVE_SCAN");
	string_as_boolean(envvar, &(allow_intrusive_scan));

	// log level
	envvar = getenv("LIBNFC_LOG_LEVEL");
	if (envvar) {
		log_level = atoi(envvar);
	}
#endif // ENVVARS

	// Initialize log before use it...
	log_init(this);

	// Debug context state
#if defined DEBUG
	log_put(LOG_GROUP, LOG_CATEGORY, NFC_LOG_PRIORITY_NONE,  "log_level is set to %"PRIu32, log_level);
#else
	log_put(LOG_GROUP, LOG_CATEGORY, NFC_LOG_PRIORITY_DEBUG,  "log_level is set to %"PRIu32, log_level);
#endif
	log_put(LOG_GROUP, LOG_CATEGORY, NFC_LOG_PRIORITY_DEBUG, "allow_autoscan is set to %s", (allow_autoscan) ? "true" : "false");
	log_put(LOG_GROUP, LOG_CATEGORY, NFC_LOG_PRIORITY_DEBUG, "allow_intrusive_scan is set to %s", (allow_intrusive_scan) ? "true" : "false");

	log_put(LOG_GROUP, LOG_CATEGORY, NFC_LOG_PRIORITY_DEBUG, "%d device(s) defined by user", user_defined_device_count);
	for (uint32_t i = 0; i < user_defined_device_count; i++) {
		log_put(LOG_GROUP, LOG_CATEGORY, NFC_LOG_PRIORITY_DEBUG, "  #%d name: \"%s\", connstring: \"%s\"", i, user_defined_devices[i].name, user_defined_devices[i].connstring);
	}
}

NfcContext::~NfcContext()
{
	log_exit();
}


#ifndef CONFIG_H
#define CONFIG_H

#define HAVE_SYS_TYPES_H
#define HAVE_ENDIAN_H
#define HAVE_BYTESWAP_H
#define WITH_DEBUG

#endif // CONFIG_H


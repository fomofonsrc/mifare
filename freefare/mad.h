#ifndef __MAD_H
#define __MAD_H

#include "mifare_classic.h"

struct mad;
typedef struct mad *Mad;

Mad		mad_new (const uint8_t version);
Mad		mad_read (MifareClassicTag*);
int		mad_write (MifareClassicTag*, Mad,
				   const MifareClassicKey &key_b_sector_00,
				   const MifareClassicKey &key_b_sector_10);

int		mad_get_version (Mad);
void	mad_set_version (Mad, const uint8_t version);
uint8_t mad_get_card_publisher_sector (Mad);
int		mad_set_card_publisher_sector (Mad mad, const uint8_t cps);
int		mad_get_aid (Mad, const uint8_t sector, MadAid *aid);
int		mad_set_aid (Mad, const uint8_t sector, MadAid aid);
bool	mad_sector_reserved (const uint8_t sector);
void	mad_free (Mad);

uint8_t *mifare_application_alloc (Mad, const MadAid, const size_t size);
int	mifare_application_free (Mad, const MadAid);
uint8_t *mifare_application_find (Mad, const MadAid);

#endif // __MAD_H


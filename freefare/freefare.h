/*-
 * Copyright (C) 2009, 2010, Romain Tartiere, Romuald Conty.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef __FREEFARE_H__
#define __FREEFARE_H__

#include <list>
#include <stdint.h>

#include "nfc.h"

class FreefareTag
{
public:
	enum Type {
		NONE,
		FELICA,
		MIFARE_MINI,
		MIFARE_CLASSIC_1K,
		MIFARE_CLASSIC_4K,
		MIFARE_DESFIRE,
		MIFARE_ULTRALIGHT
	};
	typedef std::list<FreefareTag*> TagList;

	FreefareTag(NfcDevice*, const NfcTarget&);
	virtual ~FreefareTag() {}

	virtual int	connect() { return -1; }
	virtual int	disconnect() { return 0; }
	virtual int	read(const size_t offset, uint8_t* data, size_t length) = 0;
	virtual int	write(const size_t offset, const uint8_t* data, size_t length) = 0;
	virtual bool authenticate (const uint8_t*) { return false; }
	virtual bool changeKey (const uint8_t*) { return false; }

	virtual uint64_t tag_uid() const { return 0; }
	virtual bool run() { return false; }
	virtual Type type() const { return NONE; }
	virtual const char* friendly_name() const  { return ""; }

	static bool	selected_tag_is_present(NfcDevice *device);

	const char* strerror ();

	NfcDevice *device;
	NfcTarget info;
	bool active;

	static Type detect(NfcTarget);
};

struct MadAid {
	uint8_t application_code;
	uint8_t function_cluster_code;
};

#endif /* !__FREEFARE_H__ */

/*-
 * Copyright (C) 2009, 2010, Romain Tartiere, Romuald Conty.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/*
 * This implementation was written based on information provided by the
 * following document:
 *
 * AN10787
 * MIFARE Application Directory (MAD)
 * Rev. 04 - 5 March 2009
 *
 * NXP Type MF1K/4K Tag Operation
 * Storing NFC Forum data in Mifare Standard 1k/4k
 * Rev. 1.1 - 21 August 2007
 */
#include "config.h"

#include <sys/types.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "mad.h"
#include "mifare_classic.h"
#include "freefare_internal.h"

/*
 * The documentation says the preset is 0xE3 but the bits have to be mirrored:
 * 0xe3 = 1110 0011 <=> 1100 0111 = 0xc7
 */
#define CRC_PRESET 0xc7

#define SECTOR_0X00_AIDS 15
#define SECTOR_0X10_AIDS 23

struct mad_sector_0x00 {
    uint8_t crc;
    uint8_t info;
    MadAid aids[SECTOR_0X00_AIDS];
};

struct mad_sector_0x10 {
    uint8_t crc;
    uint8_t info;
    MadAid aids[SECTOR_0X10_AIDS];
};

struct mad {
    struct mad_sector_0x00 sector_0x00;
    struct mad_sector_0x10 sector_0x10;
    uint8_t version;
};

/* Public Key A value of MAD sector(s) */
const MifareClassicKey mad_public_key_a = {
	0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5
};

/* AID - Administration codes: */
/* if sector is free */
const MadAid mad_free_aid = {0,0};
/* if sector is defect, e.g. access keys are destroyed or unknown */
const MadAid mad_defect_aid = {0,1};
/* if sector is reserved */
const MadAid mad_reserved_aid = {0,2};
/* if sector contains card holder information in ASCII format. */
const MadAid mad_card_holder_aid = {0,4};
/* if sector not applicable (above memory size) */
const MadAid mad_not_applicable_aid = {0,5};

/* NFC Forum AID */
const MadAid mad_nfcforum_aid = { 0xe1, 0x03 };

/*
 * Allocate an empty new MAD.
 */
Mad mad_new (uint8_t version)
{
	Mad mad = (Mad)malloc (sizeof (*mad));

    if (!mad)
        return NULL;

    mad->version = version;
    memset (&(mad->sector_0x00), 0, sizeof (mad->sector_0x00));
    memset (&(mad->sector_0x10), 0, sizeof (mad->sector_0x10));

    return mad;
}

/*
 * Compute CRC.
 */
void
nxp_crc (uint8_t *crc, const uint8_t value)
{
    /* x^8 + x^4 + x^3 + x^2 + 1 => 0x11d */
    const uint8_t poly = 0x1d;

    *crc ^= value;
    for (int current_bit = 7; current_bit >= 0; current_bit--) {
        int bit_out = (*crc) & 0x80;
        *crc <<= 1;
        if (bit_out)
            *crc ^= poly;

    }
}

uint8_t
sector_0x00_crc8 (Mad mad)
{
    uint8_t crc = CRC_PRESET;

    nxp_crc (&crc, mad->sector_0x00.info);

    for (int n = 0; n < SECTOR_0X00_AIDS; n++) {
        nxp_crc (&crc, mad->sector_0x00.aids[n].application_code);
        nxp_crc (&crc, mad->sector_0x00.aids[n].function_cluster_code);
    }

    return crc;
}

uint8_t
sector_0x10_crc8 (Mad mad)
{
    uint8_t crc = CRC_PRESET;

    nxp_crc (&crc, mad->sector_0x10.info);

    for (int n = 0; n < SECTOR_0X10_AIDS; n++) {
        nxp_crc (&crc, mad->sector_0x10.aids[n].application_code);
        nxp_crc (&crc, mad->sector_0x10.aids[n].function_cluster_code);
    }

    return crc;
}

/*
 * Read a MAD from the provided MIFARE tag.
 */
Mad mad_read (MifareClassicTag* tag)
{
	Mad mad = (Mad)malloc (sizeof (*mad));

    if (!mad)
		return 0;

    /* Authenticate using MAD key A */
	if (!tag->authenticate (0x03, mad_public_key_a, MFC_KEY_A)) {
		free (mad);
		return NULL;
	}

    /* Read first sector trailer block */
    MifareClassicBlock data;
	if (tag->readBlock(0x03, &data) < 0) {
		free (mad);
		return NULL;
	}
    uint8_t gpb = data[9];

    /* Check MAD availability (DA bit) */
    if (!(gpb & 0x80)) {
		free (mad);
		return NULL;
	}

    /* Get MAD version (ADV bits) */
    switch (gpb & 0x03) {
    case 0x01:
        mad->version = 1;
        break;
    case 0x02:
        mad->version = 2;
        break;
    default:
        /* MAD enabled but version not supported */
        errno = ENOTSUP;
		free (mad);
		return NULL;
	}

    /* Read MAD data at 0x00 (MAD1, MAD2) */
	if (tag->readBlock(0x01, &data) < 0) {
		free (mad);
		return NULL;
	}

    uint8_t *p = (uint8_t *) &(mad->sector_0x00);
    memcpy (p, data, sizeof (data));

    p+= sizeof (data);

	if (tag->readBlock(0x02, &data) < 0) {
		free (mad);
		return NULL;
	}

	memcpy (p, data, sizeof (data));

    uint8_t crc = mad->sector_0x00.crc;
    uint8_t computed_crc = sector_0x00_crc8 (mad);
	if (crc != computed_crc) {
		free (mad);
		return NULL;
	}

    /* Read MAD data at 0x10 (MAD2) */
    if (mad->version == 2) {

        /* Authenticate using MAD key A */
		if (!tag->authenticate (0x43, mad_public_key_a, MFC_KEY_A)) {
			free (mad);
			return NULL;
		}

        p = (uint8_t *) &(mad->sector_0x10);

		if (tag->readBlock (0x40, &data) < 0) {
			free (mad);
			return NULL;
		}
        memcpy (p, data, sizeof (data));

        p += sizeof (data);

		if (tag->readBlock (0x41, &data) < 0) {
			free (mad);
			return NULL;
		}
        memcpy (p, data, sizeof (data));

        p += sizeof (data);

		if (tag->readBlock (0x42, &data) < 0) {
			free (mad);
			return NULL;
		}
        memcpy (p, data, sizeof (data));

        crc = mad->sector_0x10.crc;
        computed_crc = sector_0x10_crc8 (mad);
		if (crc != computed_crc) {
			free (mad);
			return NULL;
		}
    }

    return mad;
}

/*
 * Write the mad to the provided MIFARE tad using the provided Key-B keys.
 */
int mad_write (MifareClassicTag* tag,
			   Mad mad,
			   const MifareClassicKey& key_b_sector_00,
			   const MifareClassicKey& key_b_sector_10)
{
    MifareClassicBlock data;

	if (!tag->authenticate (0x00, key_b_sector_00, MFC_KEY_B))
        return -1;

	if ((1 != tag->get_data_block_permission (0x01, MCAB_W, MFC_KEY_B)) ||
			(1 != tag->get_data_block_permission (0x02, MCAB_W, MFC_KEY_B)) ||
			(1 != tag->get_trailer_block_permission (0x03, MCAB_WRITE_KEYA, MFC_KEY_B)) ||
			(1 != tag->get_trailer_block_permission (0x03, MCAB_WRITE_ACCESS_BITS, MFC_KEY_B))) {
        errno = EPERM;
        return -1;
    }

    uint8_t gpb = 0x80;

    /*
     * FIXME Handle mono-application cards
     */
    gpb |= 0x40;

    /* Write MAD version */
    switch (mad->version) {
    case 1:
        gpb |= 0x01;
        break;
    case 2:
        gpb |= 0x02;
        break;
    }

    if (2 == mad->version) {
		if (!tag->authenticate (0x40, key_b_sector_10, MFC_KEY_B))
            return -1;

		if ((1 != tag->get_data_block_permission (0x40, MCAB_W, MFC_KEY_B)) ||
				(1 != tag->get_data_block_permission (0x41, MCAB_W, MFC_KEY_B)) ||
				(1 != tag->get_data_block_permission (0x42, MCAB_W, MFC_KEY_B)) ||
				(1 != tag->get_trailer_block_permission (0x43, MCAB_WRITE_KEYA, MFC_KEY_B)) ||
				(1 != tag->get_trailer_block_permission (0x43, MCAB_WRITE_ACCESS_BITS, MFC_KEY_B))) {
            errno = EPERM;
            return -1;
        }

        mad->sector_0x10.crc = sector_0x10_crc8 (mad);

        memcpy (data, (uint8_t *)&(mad->sector_0x10), sizeof (data));
		if (tag->writeBlock(0x40, data) < 0) return -1;
        memcpy (data, (uint8_t *)&(mad->sector_0x10) + sizeof (data), sizeof (data));
		if (tag->writeBlock(0x41, data) < 0) return -1;
        memcpy (data, (uint8_t *)&(mad->sector_0x10) + sizeof (data) * 2, sizeof (data));
		if (tag->writeBlock(0x42, data) < 0) return -1;

		tag->trailer_block (&data, mad_public_key_a, 0x0, 0x1, 0x1, 0x6, 0x00, key_b_sector_10);
		if (tag->writeBlock(0x43, data) < 0) return -1;

    }

    mad->sector_0x00.crc = sector_0x00_crc8 (mad);

	if (!tag->authenticate (0x00, key_b_sector_00, MFC_KEY_B)) return -1;
    memcpy (data, (uint8_t *)&(mad->sector_0x00), sizeof (data));
	if (tag->writeBlock(0x01, data) < 0) return -1;
    memcpy (data, (uint8_t *)&(mad->sector_0x00) + sizeof (data), sizeof (data));
	if (tag->writeBlock(0x02, data) < 0) return -1;

	tag->trailer_block (&data, mad_public_key_a, 0x0, 0x1, 0x1, 0x6, gpb, key_b_sector_00);
	if (tag->writeBlock(0x03, data) < 0) return -1;

    return 0;
}

/*
 * Return a MAD version.
 */
int
mad_get_version (Mad mad)
{
    return mad->version;
}

/*
 * Set a MAD version.
 */
void
mad_set_version (Mad mad, const uint8_t version)
{
    if ((version == 2) && (mad->version == 1)) {
        /* We use a larger MAD so initialise the new blocks */
        memset (&(mad->sector_0x10), 0, sizeof (mad->sector_0x10));
    }
    mad->version = version;
}

/*
 * Return the MAD card publisher sector.
 */
uint8_t mad_get_card_publisher_sector(Mad mad)
{
    return (mad->sector_0x00.info & 0x3f);
}

/*
 * Set the MAD card publisher sector.
 */
int mad_set_card_publisher_sector(Mad mad, const uint8_t cps)
{
    if (((mad->version == 2) && (cps > 0x27)) | (mad->version == 1) && (cps > 0x0f)) {
        errno = EINVAL;
        return -1;
    }

    mad->sector_0x00.info = (cps & 0x3f);
    return 0;
}

/*
 * Get the provided sector's application identifier.
 */
int mad_get_aid(Mad mad, const uint8_t sector, MadAid *aid)
{
    if ((sector < 1) || (sector == 0x10) || (sector > 0x27)) {
        errno = EINVAL;
        return -1;
    }

    if (sector > 0x0f) {
        if (mad->version != 2) {
            errno = EINVAL;
            return -1;
        }

        aid->function_cluster_code = mad->sector_0x10.aids[sector - 0x0f - 2].function_cluster_code;
        aid->application_code      = mad->sector_0x10.aids[sector - 0x0f - 2].application_code;
    } else {
        aid->function_cluster_code = mad->sector_0x00.aids[sector - 1].function_cluster_code;
        aid->application_code      = mad->sector_0x00.aids[sector - 1].application_code;
    }

    return 0;
}

/*
 * Set the provided sector's application identifier.
 */
int mad_set_aid(Mad mad, const uint8_t sector, MadAid aid)
{
    if ((sector < 1) || (sector == 0x10) || (sector > 0x27)) {
        errno = EINVAL;
        return -1;
    }

    if (sector > 0x0f) {
        if (mad->version != 2) {
            errno = EINVAL;
            return -1;
        }
        mad->sector_0x10.aids[sector - 0x0f - 2].function_cluster_code = aid.function_cluster_code;
        mad->sector_0x10.aids[sector - 0x0f - 2].application_code      = aid.application_code;
    } else {
        mad->sector_0x00.aids[sector - 1].function_cluster_code = aid.function_cluster_code;
        mad->sector_0x00.aids[sector - 1].application_code      = aid.application_code;
    }

    return 0;
}

bool mad_sector_reserved (const uint8_t sector)
{
    return ((0x00 == sector) || (0x10 == sector));
}

/*
 * Free memory allocated by mad_new() and mad_read().
 */
void mad_free (Mad mad)
{
    free (mad);
}

#define FIRST_SECTOR 1

int	 aidcmp (const MadAid left, const MadAid right);
size_t	 count_aids (const Mad mad, const MadAid aid);

/*
 * Get the number of sectors allocated in the MAD for the provided application.
 */
size_t count_aids (const Mad mad, const MadAid aid)
{
	size_t result = 0;

	uint8_t s_max = (mad_get_version (mad) == 1) ? 0x0f : 0x27;

	/* Count application sectors */
	MadAid c_aid;
	for (uint8_t s = FIRST_SECTOR; s <= s_max; s++) {
		mad_get_aid (mad, s, &c_aid);
		if (0 == aidcmp (aid, c_aid)) {
			result++;
		}
	}

	return result;
}

/*
 * Compare two application identifiers.
 */
inline int aidcmp (const MadAid left, const MadAid right)
{
	return ((left.function_cluster_code - right.function_cluster_code) << 8) | (left.application_code - right.application_code);
}


/*
 * Card publisher functions (MAD owner).
 */

/*
 * Allocates a new application into a MAD.
 */
uint8_t* mifare_application_alloc (Mad mad, MadAid aid, size_t size)
{
	uint8_t sector_map[40];
	uint8_t sector;
	MadAid sector_aid;
	uint8_t *res = NULL;
	ssize_t s = size;

	/*
	 * Ensure the card does not already have the application registered.
	 */
	uint8_t *found;
	if ((found = mifare_application_find (mad, aid))) {
		free (found);
		return NULL;
	}

	for (size_t i = 0; i < sizeof (sector_map); i++)
		sector_map[i] = 0;

	/*
	 * Try to minimize lost space and allocate as many large pages as possible
	 * when the target is a Mifare Classic 4k.
	 */
	MadAid free_aid = { 0x00, 0x00 };
	if (mad_get_version (mad) == 2) {
		sector = 32;
		while ((s >= 12*16) && sector < 40) {
			mad_get_aid (mad, sector, &sector_aid);
			if (0 == aidcmp (sector_aid, free_aid)) {
				sector_map[sector] = 1;
				s -= 15*16;
			}
			sector++;
		}
	}

	sector = FIRST_SECTOR;
	uint8_t s_max = (mad_get_version (mad) == 1) ? 15 : 31;
	while ((s > 0) && (sector <= s_max)) {
		if (mad_sector_reserved (sector))
			continue;
		mad_get_aid (mad, sector, &sector_aid);
		if (0 == aidcmp (sector_aid, free_aid)) {
			sector_map[sector] = 1;
			s -= 3*16;
		}
		sector++;
	}

	/*
	 * Ensure the remaining free space is suficient before destroying the MAD.
	 */
	if (s > 0)
		return NULL;

	int n = 0;
	for (size_t i = FIRST_SECTOR; i < sizeof (sector_map); i++)
		if (sector_map[i])
			n++;

	if (!(res = (uint8_t*)malloc (sizeof (*res) * (n+1))))
		return NULL;

	n = 0;
	for (size_t i = FIRST_SECTOR; i < sizeof (sector_map); i++)
		if (sector_map[i]) {
			res[n] = i;
			mad_set_aid (mad, i, aid);
			n++;
		}

	res[n] = 0;

	/* Return the list of allocated sectors */
	return res;
}

/*
 * Remove an application from a MAD.
 */
int mifare_application_free (Mad mad, MadAid aid)
{
	uint8_t *sectors = mifare_application_find (mad, aid);
	uint8_t *p = sectors;
	MadAid free_aid = { 0x00, 0x00 };

	/* figure out if malloc() in mifare_application_find() failed */
	if (sectors == NULL) return count_aids (mad, aid) ? -1 : 0;

	while (*p) {
		mad_set_aid (mad, *p, free_aid);
		p++;
	}

	free (sectors);

	return 0;
}


/*
 * Application owner functions.
 */

/*
 * Get all sector numbers of an application from the provided MAD.
 */
uint8_t* mifare_application_find (Mad mad, MadAid aid)
{
	uint8_t *res = NULL;
	size_t res_count = count_aids (mad, aid);

	if (res_count)
		res = (uint8_t*)malloc (sizeof (*res) * (res_count + 1));

	size_t r = FIRST_SECTOR, w = 0;
	if (res) {
		/* Fill in the result */
		MadAid c_aid;
		while (w < res_count) {
			mad_get_aid (mad, r, &c_aid);
			if (0 == aidcmp (c_aid, aid)) {
				res[w++] = r;
			}
			r++;
		}
		res[w] = 0;
	}

	return res;
}

TARGET = freefare
TEMPLATE = lib
CONFIG += staticlib
CONFIG -= qt

QMAKE_CFLAGS += -std=gnu99

INCLUDEPATH += ../include

SOURCES = felica.cpp \
    freefare.cpp \
    mifare_classic.cpp \
    mifare_ultralight.cpp \
    mifare_desfire.cpp \
    mifare_desfire_aid.cpp \
    mifare_desfire_crypto.cpp \
    mifare_desfire_error.cpp \
    mifare_desfire_key.cpp \
    mad.cpp \
    tlv.cpp \
    hexdump.cpp \
    mifare_ulc.cpp

HEADERS += \
    freefare.h \
    freefare_internal.h \
    config.h \
    libutil.h \
    mifare_desfire.h \
    felica.h \
    mad.h \
    tlv.h \
    mifare_classic.h \
    mifare_ultralight.h \
    mifare_desfire_key.h \
    mifare_desfire_crypto.h \
    mifare_ulc.h

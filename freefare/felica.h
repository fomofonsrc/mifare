#ifndef __FELICA_H
#define __FELICA_H

#include "freefare.h"

#define FELICA_SC_RW 0x0009
#define FELICA_SC_RO 0x000b

class Felica : public FreefareTag
{
public:
	uint64_t tag_uid() const;
	const char* friendly_name() const {return "FeliCA";}

	ssize_t read (uint16_t service, uint8_t block, uint8_t *data, size_t length);
	ssize_t read_ex (uint16_t service, uint8_t block_count, uint8_t blocks[], uint8_t *data, size_t length);
	ssize_t write (uint16_t service, uint8_t block, const uint8_t *data, size_t length);
	ssize_t write_ex (uint16_t service, uint8_t block_count, uint8_t blocks[], const uint8_t *data, size_t length);

private:
	ssize_t felica_transceive (uint8_t *data_in, uint8_t *data_out, size_t data_out_length);
};

#endif // __FELICA_H



#include <sys/types.h>
#include <errno.h>
#include <string.h>

#include "mifare_ultralight.h"
#include "freefare_internal.h"

MifareUltralightTag::MifareUltralightTag(NfcDevice* dev, const NfcTarget &tgt) :
	FreefareTag(dev, tgt)
{

}

uint64_t MifareUltralightTag::tag_uid() const
{
	uint64_t res = 0;
	for (size_t i = 0; i < 8 && i < info.nti.nai.szUidLen; i++) {
		res <<= 8;
		res |= info.nti.nai.abtUid[i];
	}
	return res;
}

#define MIFARE_ULTRALIGHT_PAGE_COUNT  16

bool MifareUltralightTag::is_page_valid(uint8_t page)
{
	if (page >= MIFARE_ULTRALIGHT_PAGE_COUNT) {
		return false;
	}
	return true;
}

/*
 * Establish connection to the provided tag.
 */
int MifareUltralightTag::connect()
{
	if (active) return -1;

	NfcTarget pnti;
	nfc_modulation modulation;
	modulation.nmt = NMT_ISO14443A;
	modulation.nbr = NBR_106;

	if (nfc_initiator_select_passive_target(device, modulation,
											info.nti.nai.abtUid,
											info.nti.nai.szUidLen,
											&pnti) >= 0)
	{
		active = 1;
		return 0;
	} else {
		return -1;
	}
}

/*
 * Terminate connection with the provided tag.
 */
int MifareUltralightTag::disconnect()
{
	if (!active) return -1;

	if (nfc_initiator_deselect_target (device) >= 0) {
		active = 0;
		return 0;
	} else {
		return -1;
	}
}

/*
 * Read data
 */

int MifareUltralightTag::read(const size_t offset, uint8_t *data, size_t length)
{
	uint8_t page = offset / 4;
	if (!is_page_valid (page)) return -1;
	return readPage(page, data);
}

/*
 * Write data
 */

int MifareUltralightTag::write(const size_t offset, const uint8_t *data, size_t length)
{
	uint8_t page = offset / 4;
	if (!is_page_valid (page)) return -1;
	return writePage(page, data);
}

/*
 * Read block of 4 pages (16 bytes)
 */

int MifareUltralightTag::readPage(const uint8_t page, uint8_t *data)
{
	if (!active) return -1;

	uint8_t cmd[2];
	cmd[0] = 0x30;
	cmd[1] = page;

	return nfc_initiator_transceive_bytes (device, cmd, sizeof(cmd), data, 16, 0);
}

/*
 * Write one page
 */

int MifareUltralightTag::writePage(const uint8_t page, const uint8_t *data)
{
	if (!active) return -1;

	uint8_t res[1];
	uint8_t cmd[6];
	cmd[0] = 0xA2;
	cmd[1] = page;
	memcpy(&cmd[2], data, 4);

	int ret = nfc_initiator_transceive_bytes (device, cmd, sizeof(cmd), res, sizeof(res), 0);
	return (ret < 0 ? ret : 4);
}

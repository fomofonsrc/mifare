/*-
 * Copyright (C) 2010, Romain Tartiere, Romuald Conty.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <stdlib.h>
#include <string.h>

#include "freefare.h"
#include "freefare_internal.h"

#define MAX_CANDIDATES 16

#define NXP_MANUFACTURER_CODE 0x04

struct SupportedTag {
	FreefareTag::Type type;
	uint8_t modulation_type;
	uint8_t SAK;
	uint8_t ATS_min_length;
	uint8_t ATS_compare_length;
	uint8_t ATS[5];
};

SupportedTag supported_tags[] = {
	{ FreefareTag::FELICA,              NMT_FELICA,    0x00, 0, 0, { 0x00 }},
	{ FreefareTag::MIFARE_MINI,			NMT_ISO14443A, 0x09, 0, 0, { 0x00 }},
	{ FreefareTag::MIFARE_CLASSIC_1K,   NMT_ISO14443A, 0x08, 0, 0, { 0x00 }},
	{ FreefareTag::MIFARE_CLASSIC_1K,   NMT_ISO14443A, 0x28, 0, 0, { 0x00 }},	// "Mifare Classic 1k (Emulated)"
	{ FreefareTag::MIFARE_CLASSIC_1K,   NMT_ISO14443A, 0x6A, 0, 0, { 0x00 }},	// "Mifare Classic 1k (Emulated)"
	{ FreefareTag::MIFARE_CLASSIC_1K,   NMT_ISO14443A, 0x88, 0, 0, { 0x00 }},	// "Infineon Mifare Classic 1k"
	{ FreefareTag::MIFARE_CLASSIC_4K,   NMT_ISO14443A, 0x18, 0, 0, { 0x00 }},	// "Mifare Classic 4k"
	{ FreefareTag::MIFARE_CLASSIC_4K,   NMT_ISO14443A, 0x38, 0, 0, { 0x00 }},	// "Mifare Classic 4k (Emulated)"
	{ FreefareTag::MIFARE_DESFIRE,      NMT_ISO14443A, 0x20, 5, 4, { 0x75, 0x77, 0x81, 0x02 /*, 0xXX */ }},	// "Mifare DESFire"
	{ FreefareTag::MIFARE_DESFIRE,      NMT_ISO14443A, 0x60, 4, 3, { 0x78, 0x33, 0x88 /*, 0xXX */ }},	// "Cyanogenmod card emulation"
	{ FreefareTag::MIFARE_DESFIRE,      NMT_ISO14443A, 0x60, 4, 3, { 0x78, 0x80, 0x70 /*, 0xXX */ }},	// "Android HCE"
	{ FreefareTag::MIFARE_ULTRALIGHT,   NMT_ISO14443A, 0x00, 0, 0, { 0x00 }},
};

FreefareTag::FreefareTag(NfcDevice* dev, const NfcTarget &tgt) :
	device(dev),
	info(tgt),
	active(false)
{
}


FreefareTag::Type FreefareTag::detect(NfcTarget target)
{
	/* Ensure the target is supported */
	for (size_t i = 0; i < sizeof (supported_tags) / sizeof (struct SupportedTag); i++) {
		SupportedTag& st = supported_tags[i];
		if (target.nm.nmt != st.modulation_type)
			continue;

		if (target.nm.nmt == NMT_FELICA) {
			return st.type;
		}
		if (target.nm.nmt == NMT_ISO14443A) {
			if ((target.nti.nai.szUidLen == 4 || target.nti.nai.szUidLen == 7 || target.nti.nai.abtUid[0] == NXP_MANUFACTURER_CODE)
				&& target.nti.nai.btSak == st.SAK)
			{
				if (st.ATS_min_length) {
					if (target.nti.nai.szAtsLen < st.ATS_min_length) {
						break;
					}
					if (memcmp(target.nti.nai.abtAts, st.ATS, st.ATS_compare_length)) {
						break;
					}
				}
				return st.type;
			}
		}
	}
	return NONE;
}

/*
 * Returns true if last selected tag is still present.
 */
bool FreefareTag::selected_tag_is_present(NfcDevice *device)
{
    return (nfc_initiator_target_is_present(device, NULL) == NFC_SUCCESS);
}

const char* FreefareTag::strerror ()
{
    const char *p = "Unknown error";
	if (nfc_device_get_last_error (device) < 0) {
		p = nfc_strerror (device);
	}
    return p;
}

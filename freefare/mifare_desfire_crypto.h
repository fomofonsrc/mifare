#ifndef MIFARE_DESFIRE_CRYPTO_H
#define MIFARE_DESFIRE_CRYPTO_H

#include "mifare_desfire_key.h"

#define MDCM_MASK 0x000F

#define CMAC_NONE 0

// Data send to the PICC is used to update the CMAC
#define CMAC_COMMAND 0x010
// Data received from the PICC is used to update the CMAC
#define CMAC_VERIFY  0x020

// MAC the command (when MDCM_MACED)
#define MAC_COMMAND 0x100
// The command returns a MAC to verify (when MDCM_MACED)
#define MAC_VERIFY  0x200

#define ENC_COMMAND 0x1000
#define NO_CRC      0x2000

#define MAC_MASK   0x0F0
#define CMAC_MACK  0xF00

class FreefareTag;

typedef enum {
	MCD_SEND,
	MCD_RECEIVE
} MifareCryptoDirection;

typedef enum {
	MCO_ENCYPHER,
	MCO_DECYPHER
} MifareCryptoOperation;


uint8_t		*mifare_cryto_preprocess_data (FreefareTag* tag,
										   uint8_t* data,
										   int *nbytes,
										   off_t offset,
										   int communication_settings);

uint8_t		*mifare_cryto_postprocess_data (FreefareTag* tag,
											uint8_t *data,
											ssize_t *nbytes,
											int communication_settings);

void		 mifare_cypher_single_block (MifareDESFireKey key, uint8_t *data, uint8_t *ivect, MifareCryptoDirection direction, MifareCryptoOperation operation, size_t block_size);
void		 mifare_cypher_blocks_chained (FreefareTag* tag, MifareDESFireKey key, uint8_t *ivect, uint8_t *data, size_t data_size, MifareCryptoDirection direction, MifareCryptoOperation operation);
void		 rol (uint8_t *data, const size_t len);
void		 desfire_crc32 (const uint8_t *data, const size_t len, uint8_t *crc);
void		 desfire_crc32_append (uint8_t *data, const size_t len);
size_t		 key_block_size (const MifareDESFireKey key);
size_t		 padded_data_length (const size_t nbytes, const size_t block_size);
size_t		 maced_data_length (const MifareDESFireKey key, const size_t nbytes);
size_t		 enciphered_data_length (const FreefareTag* tag, const size_t nbytes, int communication_settings);

void		 cmac_generate_subkeys (MifareDESFireKey key);
void		 cmac (const MifareDESFireKey key, uint8_t *ivect, const uint8_t *data, size_t len, uint8_t *cmac);
uint8_t		*assert_crypto_buffer_size (FreefareTag* tag, size_t nbytes);

#endif // MIFARE_DESFIRE_CRYPTO_H


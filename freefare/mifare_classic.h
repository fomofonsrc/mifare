#ifndef __MIFARE_CLASSIC_H
#define __MIFARE_CLASSIC_H

#include "freefare.h"

/*
 * Access bits manipulation macros
 */
#define C_000 0
#define C_001 1
#define C_010 2
#define C_011 3
#define C_100 4
#define C_101 5
#define C_110 6
#define C_111 7
#define C_DEFAULT 255

/* MIFARE Classic Access Bits */
#define MCAB_R 0x8
#define MCAB_W 0x4
#define MCAB_D 0x2
#define MCAB_I 0x1

#define MCAB_READ_KEYA         0x400
#define MCAB_WRITE_KEYA        0x100
#define MCAB_READ_ACCESS_BITS  0x040
#define MCAB_WRITE_ACCESS_BITS 0x010
#define MCAB_READ_KEYB         0x004
#define MCAB_WRITE_KEYB        0x001


typedef unsigned char MifareClassicBlock[16];
typedef unsigned char MifareClassicAccessBits;
typedef unsigned char MifareClassicKey[6];

enum MifareClassicKeyType {
	MFC_KEY_A,
	MFC_KEY_B
};

class MifareClassicTag : public FreefareTag
{
public:
	MifareClassicTag(NfcDevice*, const NfcTarget&);

	int connect ();
	int disconnect ();
	uint64_t tag_uid() const;
	const char* friendly_name() const {return "Mifare Classic";}

	bool authenticate (const uint8_t block, const MifareClassicKey& key, const MifareClassicKeyType key_type);

	int read (const size_t offset, uint8_t* data, size_t length);
	int write (const size_t offset, const uint8_t* data, size_t length);

	int readBlock (const uint8_t, MifareClassicBlock*);
	int writeBlock (const uint8_t, const MifareClassicBlock);

	int init_value (const uint8_t block, const int32_t value, const uint8_t adr);
	int read_value (const uint8_t block, int32_t *value, uint8_t *adr);

	int increment (const uint8_t block, const uint32_t amount);
	int decrement (const uint8_t block, const uint32_t amount);
	int restore (const uint8_t block);
	int transfer (const uint8_t block);

	int get_trailer_block_permission (const uint8_t block, const uint16_t permission, const MifareClassicKeyType key_type);
	int get_data_block_permission (const uint8_t block, const unsigned char permission, const MifareClassicKeyType key_type);

	int format_sector (const uint8_t sector);

	static void trailer_block (MifareClassicBlock *block, const MifareClassicKey key_a, uint8_t ab_0, uint8_t ab_1, uint8_t ab_2, uint8_t ab_tb, const uint8_t gpb, const MifareClassicKey key_b);

	static uint8_t block_sector (uint8_t block);
	static uint8_t sector_first_block (uint8_t sector);
	static size_t sector_block_count (uint8_t sector);
	static uint8_t sector_last_block (uint8_t sector);

	ssize_t	application_read (struct mad*,
							  const MadAid,
							  void *buf,
							  size_t nbytes,
							  const MifareClassicKey&,
							  const MifareClassicKeyType);

	ssize_t	application_write (struct mad*,
							   const MadAid,
							   const void *buf,
							   size_t nbytes,
							   const MifareClassicKey&,
							   const MifareClassicKeyType);


	MifareClassicKeyType last_authentication_key_type;

	/* NFC Forum public key */
	static const MifareClassicKey nfcforum_public_key_a;

	/*
		 * The following block numbers are on 2 bytes in order to use invalid
		 * address and avoid false cache hit with inconsistent data.
		 */
	struct {
		int16_t sector_trailer_block_number;
		uint16_t sector_access_bits;
		int16_t block_number;
		uint8_t block_access_bits;
	} cached_access_bits;

private:
	int	get_block_access_bits(const uint8_t block, MifareClassicAccessBits *block_access_bits);
};

#endif // MIFARE_CLASSIC_H


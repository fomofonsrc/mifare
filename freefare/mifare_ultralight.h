#ifndef MIFARE_ULTRALIGHT_H
#define MIFARE_ULTRALIGHT_H

#include "freefare.h"

// Max PAGE_COUNT of the Ultralight Family:
#define MIFARE_ULTRALIGHT_MAX_PAGE_COUNT 0x30

class MifareUltralightTag : public FreefareTag
{
public:
	MifareUltralightTag(NfcDevice*, const NfcTarget&);

	int	connect();
	int	disconnect();
	int	read(const size_t offset, uint8_t* data, size_t length);
	int	write(const size_t offset, const uint8_t* data, size_t length);
	uint64_t tag_uid() const;
	const char* friendly_name() const {return "Mifare UltraLight";}

protected:
	int readPage(const uint8_t page, uint8_t *data);
	int writePage(const uint8_t page, const uint8_t *data);

private:
	bool is_page_valid(uint8_t page);
};

#endif // MIFARE_ULTRALIGHT_H


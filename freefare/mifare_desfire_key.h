#ifndef MIFARE_DESFIRE_KEY_H
#define MIFARE_DESFIRE_KEY_H

#include <openssl/des.h>

enum crypto_type {
	T_DES,
	T_3DES,
	T_3K3DES,
	T_AES
};

struct mifare_desfire_key {
	uint8_t data[24];
	crypto_type type;
	DES_key_schedule ks1;
	DES_key_schedule ks2;
	DES_key_schedule ks3;
	uint8_t cmac_sk1[24];
	uint8_t cmac_sk2[24];
	uint8_t aes_version;
};

typedef struct mifare_desfire_key *MifareDESFireKey;

class DESFireKey
{
public:
	static MifareDESFireKey session_key_new (const uint8_t rnda[], const uint8_t rndb[], MifareDESFireKey authentication_key);
	static MifareDESFireKey des_key_new (const uint8_t value[8]);
	static MifareDESFireKey des3_key_new (const uint8_t value[16]);
	static MifareDESFireKey des_key_new_with_version (const uint8_t value[8]);
	static MifareDESFireKey des3_key_new_with_version (const uint8_t value[16]);
	static MifareDESFireKey des3k3_key_new (const uint8_t value[24]);
	static MifareDESFireKey des3k3_key_new_with_version (const uint8_t value[24]);
	static MifareDESFireKey aes_key_new (const uint8_t value[16]);
	static MifareDESFireKey aes_key_new_with_version (const uint8_t value[16], uint8_t version);

	static uint8_t	 key_get_version (MifareDESFireKey key);
	static void		 key_set_version (MifareDESFireKey key, uint8_t version);
	static void		 key_free (MifareDESFireKey key);
};

#endif // MIFARE_DESFIRE_KEY_H


#ifndef MIFARE_ULC_H
#define MIFARE_ULC_H

#include "mifare_ultralight.h"

class MifareUltralightCTag : public MifareUltralightTag
{
public:
	MifareUltralightCTag(NfcDevice*, const NfcTarget&);

	int	read(const size_t offset, uint8_t* data, size_t length);
	int	write(const size_t offset, const uint8_t* data, size_t length);
	bool authenticate (const uint8_t* kdata);
	const char* friendly_name() const { return "Mifare UltraLight-C"; }
	bool changeKey(const uint8_t*);

	static bool detectULC(NfcDevice*, const NfcTarget&);
	static const uint8_t default_key[];

private:
	bool is_page_valid(uint8_t page, bool mode_write);
};


#endif // MIFARE_ULC_H

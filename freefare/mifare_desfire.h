#ifndef __MIFARE_DESFIRE_H
#define __MIFARE_DESFIRE_H

#include "freefare.h"
#include "mifare_desfire_key.h"

/* Status and error codes */

#define	OPERATION_OK		0x00
#define	NO_CHANGES		0x0C
#define	OUT_OF_EEPROM_ERROR	0x0E
#define	ILLEGAL_COMMAND_CODE	0x1C
#define	INTEGRITY_ERROR		0x1E
#define	NO_SUCH_KEY		0x40
#define	LENGTH_ERROR		0x7E
#define	PERMISSION_ERROR	0x9D
#define	PARAMETER_ERROR		0x9E
#define	APPLICATION_NOT_FOUND	0xA0
#define	APPL_INTEGRITY_ERROR	0xA1
#define	AUTHENTICATION_ERROR	0xAE
#define	ADDITIONAL_FRAME	0xAF
#define	BOUNDARY_ERROR		0xBE
#define	PICC_INTEGRITY_ERROR	0xC1
#define	COMMAND_ABORTED		0xCA
#define	PICC_DISABLED_ERROR	0xCD
#define	COUNT_ERROR		0xCE
#define	DUPLICATE_ERROR		0xDE
#define	EEPROM_ERROR		0xEE
#define	FILE_NOT_FOUND		0xF0
#define	FILE_INTEGRITY_ERROR	0xF1

/* Error code managed by the library */

#define CRYPTO_ERROR            0x01

/* File types */

enum mifare_desfire_file_types {
	MDFT_STANDARD_DATA_FILE             = 0x00,
	MDFT_BACKUP_DATA_FILE               = 0x01,
	MDFT_VALUE_FILE_WITH_BACKUP         = 0x02,
	MDFT_LINEAR_RECORD_FILE_WITH_BACKUP = 0x03,
	MDFT_CYCLIC_RECORD_FILE_WITH_BACKUP = 0x04
};

/* Communication mode */

#define MDCM_PLAIN      0x00
#define MDCM_MACED      0x01
#define MDCM_ENCIPHERED 0x03

/* Mifare DESFire EV1 Application crypto operations */

#define APPLICATION_CRYPTO_DES    0x00
#define APPLICATION_CRYPTO_3K3DES 0x40
#define APPLICATION_CRYPTO_AES    0x80

/* Access right */

#define MDAR(read,write,read_write,change_access_rights) ( \
	(read << 12) | \
	(write << 8) | \
	(read_write << 4) | \
	(change_access_rights) \
	)
#define MDAR_READ(ar)       (((ar) >> 12) & 0x0f)
#define MDAR_WRITE(ar)      (((ar) >>  8) & 0x0f)
#define MDAR_READ_WRITE(ar) (((ar) >>  4) & 0x0f)
#define MDAR_CHANGE_AR(ar)  ((ar)         & 0x0f)

#define MDAR_KEY0  0x0
#define MDAR_KEY1  0x1
#define MDAR_KEY2  0x2
#define MDAR_KEY3  0x3
#define MDAR_KEY4  0x4
#define MDAR_KEY5  0x5
#define MDAR_KEY6  0x6
#define MDAR_KEY7  0x7
#define MDAR_KEY8  0x8
#define MDAR_KEY9  0x9
#define MDAR_KEY10 0xa
#define MDAR_KEY11 0xb
#define MDAR_KEY12 0xc
#define MDAR_KEY13 0xd
#define MDAR_FREE  0xE
#define MDAR_DENY  0xF

struct mifare_desfire_aid {
	uint8_t data[3];
};

typedef struct mifare_desfire_aid *MifareDESFireAID;

struct MifareDESFireDF {
	uint32_t aid;
	uint16_t fid;
	uint8_t df_name[16];
	size_t df_name_len;
};

enum  authentication_scheme_e {
	AS_LEGACY,
	AS_NEW
};

#define MAX_CRYPTO_BLOCK_SIZE 16

struct mifare_desfire_tag {
//	struct FreefareTag __tag;

	uint8_t last_picc_error;
	uint8_t last_internal_error;
	uint8_t last_pcd_error;
	MifareDESFireKey session_key;
	authentication_scheme_e authentication_scheme;
	uint8_t authenticated_key_no;
	uint8_t ivect[MAX_CRYPTO_BLOCK_SIZE];
	uint8_t cmac[16];
	uint8_t *crypto_buffer;
	size_t crypto_buffer_size;
	uint32_t selected_application;
};

#define MIFARE_DESFIRE(tag) ((struct mifare_desfire_tag *) tag)

class MifareDesfire
{
public:
	static FreefareTag* tag_new ();
	static void tag_free(FreefareTag* tag);

	static int		 connect (FreefareTag* tag);
	static int		 disconnect (FreefareTag* tag);

	static int		 authenticate (FreefareTag* tag, uint8_t key_no, MifareDESFireKey key);
	static int		 authenticate_iso (FreefareTag* tag, uint8_t key_no, MifareDESFireKey key);
	static int		 authenticate_aes (FreefareTag* tag, uint8_t key_no, MifareDESFireKey key);
	static int		 change_key_settings (FreefareTag* tag, uint8_t settings);
	static int		 get_key_settings (FreefareTag* tag, uint8_t *settings, uint8_t *max_keys);
	static int		 change_key (FreefareTag* tag, uint8_t key_no, MifareDESFireKey new_key, MifareDESFireKey old_key);
	static int		 get_key_version (FreefareTag* tag, uint8_t key_no, uint8_t *version);
	static int		 create_application (FreefareTag* tag, MifareDESFireAID aid, uint8_t settings, uint8_t key_no);
	static int		 create_application_3k3des (FreefareTag* tag, MifareDESFireAID aid, uint8_t settings, uint8_t key_no);
	static int		 create_application_aes (FreefareTag* tag, MifareDESFireAID aid, uint8_t settings, uint8_t key_no);

	static int		 create_application_iso (FreefareTag* tag, MifareDESFireAID aid, uint8_t settings, uint8_t key_no, int want_iso_file_identifiers, uint16_t iso_file_id, uint8_t *iso_file_name, size_t iso_file_name_len);
	static int		 create_application_3k3des_iso (FreefareTag* tag, MifareDESFireAID aid, uint8_t settings, uint8_t key_no, int want_iso_file_identifiers, uint16_t iso_file_id, uint8_t *iso_file_name, size_t iso_file_name_len);
	static int		 create_application_aes_iso (FreefareTag* tag, MifareDESFireAID aid, uint8_t settings, uint8_t key_no, int want_iso_file_identifiers, uint16_t iso_file_id, uint8_t *iso_file_name, size_t iso_file_name_len);

	static int		 delete_application (FreefareTag* tag, MifareDESFireAID aid);
	static int		 get_application_ids (FreefareTag* tag, MifareDESFireAID *aids[], size_t *count);
	static int		 get_df_names (FreefareTag* tag, MifareDESFireDF *dfs[], size_t *count);
	static void	 free_application_ids (MifareDESFireAID aids[]);
	static int		 select_application (FreefareTag* tag, MifareDESFireAID aid);
	static int		 format_picc (FreefareTag* tag);
	static int		 get_version (FreefareTag* tag, struct mifare_desfire_version_info *version_info);
	static int		 free_mem (FreefareTag* tag, uint32_t *size);
	static int		 set_configuration (FreefareTag* tag, bool disable_format, bool enable_random_uid);
	static int		 set_default_key (FreefareTag* tag, MifareDESFireKey key);
	static int		 set_ats (FreefareTag* tag, uint8_t *ats);
	static int		 get_card_uid (FreefareTag* tag, char **uid);
	static int		 get_file_ids (FreefareTag* tag, uint8_t **files, size_t *count);
	static int		 get_iso_file_ids (FreefareTag* tag, uint16_t **files, size_t *count);
	static int		 get_file_settings (FreefareTag* tag, uint8_t file_no, struct mifare_desfire_file_settings *settings);
	static int		 change_file_settings (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights);
	static int		 create_std_data_file (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t file_size);
	static int		 create_std_data_file_iso (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t file_size, uint16_t iso_file_id);
	static int		 create_backup_data_file (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t file_size);
	static int		 create_backup_data_file_iso (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t file_size, uint16_t iso_file_id);
	static int		 create_value_file (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, int32_t lower_limit, int32_t upper_limit, int32_t value, uint8_t limited_credit_enable);
	static int		 create_linear_record_file (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t record_size, uint32_t max_number_of_records);
	static int		 create_linear_record_file_iso (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t record_size, uint32_t max_number_of_records, uint16_t iso_file_id);
	static int		 create_cyclic_record_file (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t record_size, uint32_t max_number_of_records);
	static int		 create_cyclic_record_file_iso (FreefareTag* tag, uint8_t file_no, uint8_t communication_settings, uint16_t access_rights, uint32_t record_size, uint32_t max_number_of_records, uint16_t iso_file_id);
	static int		 delete_file (FreefareTag* tag, uint8_t file_no);

	static ssize_t		 read_data (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, void *data);
	static ssize_t		 read_data_ex (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, void *data, int cs);
	static ssize_t		 write_data (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, const void *data);
	static ssize_t		 write_data_ex (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, const void *data, int cs);
	static int		 get_value (FreefareTag* tag, uint8_t file_no, int32_t *value);
	static int		 get_value_ex (FreefareTag* tag, uint8_t file_no, int32_t *value, int cs);
	static int		 credit (FreefareTag* tag, uint8_t file_no, int32_t amount);
	static int		 credit_ex (FreefareTag* tag, uint8_t file_no, int32_t amount, int cs);
	static int		 debit (FreefareTag* tag, uint8_t file_no, int32_t amount);
	static int		 debit_ex (FreefareTag* tag, uint8_t file_no, int32_t amount, int cs);
	static int		 limited_credit (FreefareTag* tag, uint8_t file_no, int32_t amount);
	static int		 limited_credit_ex (FreefareTag* tag, uint8_t file_no, int32_t amount, int cs);
	static ssize_t		 write_record (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, void *data);
	static ssize_t		 write_record_ex (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, void *data, int cs);
	static ssize_t		 read_records (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, void *data);
	static ssize_t		 read_records_ex (FreefareTag* tag, uint8_t file_no, off_t offset, size_t length, void *data, int cs);
	static int		 clear_record_file (FreefareTag* tag, uint8_t file_no);
	static int		 commit_transaction (FreefareTag* tag);
	static int		 abort_transaction (FreefareTag* tag);

	static const char*	error_lookup (uint8_t error);
	static uint8_t last_pcd_error (FreefareTag* tag);
	static uint8_t last_picc_error (FreefareTag* tag);
	static const char* strerror (FreefareTag *tag);
};

MifareDESFireAID mifare_desfire_aid_new (uint32_t aid);
MifareDESFireAID mifare_desfire_aid_new_with_mad_aid (MadAid mad_aid, uint8_t n);
uint32_t	 mifare_desfire_aid_get_aid (MifareDESFireAID aid);

uint8_t		 mifare_desfire_last_pcd_error (FreefareTag* tag);
uint8_t		 mifare_desfire_last_picc_error (FreefareTag* tag);

#pragma pack (push)
#pragma pack (1)
struct mifare_desfire_version_info {
	struct {
		uint8_t vendor_id;
		uint8_t type;
		uint8_t subtype;
		uint8_t version_major;
		uint8_t version_minor;
		uint8_t storage_size;
		uint8_t protocol;
	} hardware;
	struct {
		uint8_t vendor_id;
		uint8_t type;
		uint8_t subtype;
		uint8_t version_major;
		uint8_t version_minor;
		uint8_t storage_size;
		uint8_t protocol;
	} software;
	uint8_t uid[7];
	uint8_t batch_number[5];
	uint8_t production_week;
	uint8_t production_year;
};
#pragma pack (pop)

struct mifare_desfire_file_settings {
	uint8_t file_type;
	uint8_t communication_settings;
	uint16_t access_rights;
	union {
		struct {
			uint32_t file_size;
		} standard_file;
		struct {
			int32_t lower_limit;
			int32_t upper_limit;
			int32_t limited_credit_value;
			uint8_t limited_credit_enabled;
		} value_file;
		struct {
			uint32_t record_size;
			uint32_t max_number_of_records;
			uint32_t current_number_of_records;
		} linear_record_file;
	} settings;
};

#endif // MIFARE_DESFIRE


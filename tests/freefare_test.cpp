#include <stdio.h>
#include "mifareultralight.h"
#include "mifareultralightc.h"
#include "mifareclassictest.h"
#include "freefare_test.h"

#define MAX_TARGETS 16

bool FreefareTest::poll(NfcDevice *device)
{
	// Poll for a ISO14443A (MIFARE) tag
	nfc_modulation modulation;
	modulation.nmt = NMT_ISO14443A;
	modulation.nbr = NBR_106;

	NfcTarget nfc_targets[MAX_TARGETS];
	int targets_count = nfc_initiator_list_passive_targets(device, modulation, nfc_targets, MAX_TARGETS);
	if (targets_count < 0) {
		return false;
	}

	for (int c = 0; c < targets_count; c++)
	{
		const NfcTarget& target = nfc_targets[c];
		FreefareTag* tag = 0;
		switch (FreefareTag::detect(target)) {
		case FreefareTag::MIFARE_ULTRALIGHT:
			if (MifareUltralightC::detectULC(device, target)) {
				tag = new MifareUltralightC(device, target);
			} else {
				tag = new MifareUltralight(device, target);
			}
			break;

		case FreefareTag::MIFARE_MINI:
		case FreefareTag::MIFARE_CLASSIC_1K:
		case FreefareTag::MIFARE_CLASSIC_4K:
			tag = new MifareClassic(device, target);
			break;

		case FreefareTag::FELICA:
		case FreefareTag::MIFARE_DESFIRE:
		case FreefareTag::NONE:
			// not implementet yet
			break;
		}

		if (tag == 0) {
			continue;
		}

		printf("%s. UID=%llx\n",tag->friendly_name(), tag->tag_uid());

		if (tag->connect() < 0) {
			printf("Error connecting to tag\n");
			delete tag;
			continue;
		}

		tag->run();
		tag->disconnect();
		delete tag;
	}
	return true;
}

void FreefareTest::dump_block(uint8_t idx, const uint8_t* d)
{
	printf("page %02u: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", idx,
		   d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7], d[8], d[9], d[10], d[11], d[12], d[13], d[14], d[15]);
}

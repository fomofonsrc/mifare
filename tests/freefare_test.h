#ifndef FREEFARE_TEST
#define FREEFARE_TEST

#include <stdint.h>

class NfcDevice;

class FreefareTest
{
public:
	void dump_block(uint8_t idx, const uint8_t* block);
	static bool poll(NfcDevice *device);
};

#endif // FREEFARE_TEST


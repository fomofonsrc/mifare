/*-
 * Copyright (C) 2011, Romain Tartiere, Romuald Conty.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/*
 * This implementation was written based on information provided by the
 * following documents:
 *
 * Mifare Std as NFC Forum Enabled,
 * Extensions for Mifare standard 1k/4k as NFC Forum Enable Tag
 *   Application note
 *   Revision 1.1 — 21 August 2007
 *
 * NXP Type MF1K/4K Tag Operation, NXP Semiconductors [ANNFC1K4K]
 *   Application Note
 *   Revision 1.1 — 21 August 2007
 *   Document Identifier 130410
 */

#include "config.h"

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include "nfc.h"

#include "mad.h"
#include "tlv.h"

#define MIN(a,b) ((a < b) ? a: b)

#if 0
static void usage(char *progname)
{
    fprintf (stderr, "usage: %s -o FILE\n", progname);
    fprintf (stderr, "\nOptions:\n");
    fprintf (stderr, "  -y     Do not ask for confirmation\n");
    fprintf (stderr, "  -o     Extract NDEF message if available in FILE\n");
}
#endif

int main(int /*argc*/, char** /*argv*/)
{
    bool interactive = false;

    int error = 0;
	NfcDevice *device = NULL;
	FreefareTag::TagList tags;

#if 0
	char *ndef_output = NULL;
	int ch;
	while ((ch = getopt (argc, argv, "hyo:")) != -1) {
        switch (ch) {
        case 'h':
            usage(argv[0]);
            exit (EXIT_SUCCESS);
            break;
        case 'y':
            interactive = false;
            break;
        case 'o':
            ndef_output = optarg;
            break;
        case '?':
            if (optopt == 'o')
                fprintf (stderr, "Option -%c requires an argument.\n", optopt);
            /* FALLTHROUGH */
        default:
            usage (argv[0]);
            exit (EXIT_FAILURE);
        }
    }

    if (ndef_output == NULL) {
        usage (argv[0]);
        exit (EXIT_FAILURE);
    }
#endif

	FILE* message_stream;
	FILE* ndef_stream;

#if 1
	message_stream = stderr;
	ndef_stream = stdout;
#else
	message_stream = stdout;
	ndef_stream = fopen(ndef_output, "wb");
	if (!ndef_stream) {
		fprintf (stderr, "Could not open file %s.\n", ndef_output);
		exit (EXIT_FAILURE);
	}
#endif

    nfc_connstring devices[8];
    size_t device_count;

	NfcContext *context;
    nfc_init (&context);
    if (context == NULL)
        errx(EXIT_FAILURE, "Unable to init libnfc (malloc)");

    device_count= nfc_list_devices (context, devices, 8);
    if (device_count <= 0)
        errx (EXIT_FAILURE, "No NFC device found.");

    for (size_t d = 0; d < device_count; d++) {
        device = nfc_open (context, devices[d]);
        if (!device) {
            warnx ("nfc_open() failed.");
            error = EXIT_FAILURE;
            continue;
        }

		FreefareTag::get_tags(device, tags);
		for (FreefareTag::TagList::const_iterator it = tags.begin(); it != tags.end(); ++it)
		{
			FreefareTag* ft = *it;
			MifareClassicTag* tag = 0;
			switch (ft->type()) {
			case FreefareTag::MIFARE_CLASSIC_1K:
			case FreefareTag::MIFARE_CLASSIC_4K:
				tag = static_cast<MifareClassicTag*>(ft);
                break;
            default:
                continue;
            }

			char *tag_uid = tag->get_tag_uid();
            char buffer[BUFSIZ];

			fprintf (message_stream, "Found %s with UID %s. ", tag->friendly_name(), tag_uid);

            bool read_ndef = true;
            if (interactive) {
                fprintf (message_stream, "Read NDEF [yN] ");
                fgets (buffer, BUFSIZ, stdin);
                read_ndef = ((buffer[0] == 'y') || (buffer[0] == 'Y'));
            } else {
                fprintf (message_stream, "\n");
            }

            if (read_ndef) {
                // NFCForum card has a MAD, load it.
				if (tag->connect()) {
                    nfc_perror (device, "mifare_classic_connect");
                    error = EXIT_FAILURE;
                    goto error;
                }

				Mad mad = mad_read(tag);
				if (mad) {
                    // Dump the NFCForum application using MAD information
                    uint8_t buffer[4096];
					const MadAid mad_nfcforum_aid = { 0xe1, 0x03 };
					ssize_t len = tag->application_read (mad,
														 mad_nfcforum_aid,
														 buffer,
														 sizeof(buffer),
														 MifareClassicTag::nfcforum_public_key_a,
														 MFC_KEY_A);
					if (len != -1) {
                        uint8_t tlv_type;
                        uint16_t tlv_data_len;
                        uint8_t * tlv_data;
                        uint8_t * pbuffer = buffer;
decode_tlv:
                        tlv_data = tlv_decode (pbuffer, &tlv_type, &tlv_data_len);
                        switch (tlv_type) {
                        case 0x00:
                            fprintf (message_stream, "NFC Forum application contains a \"NULL TLV\", Skipping...\n");	// According to [ANNFC1K4K], we skip this Tag to read further TLV blocks.
                            pbuffer += tlv_record_length(pbuffer, NULL, NULL);
                            if (pbuffer >= buffer + sizeof(buffer)) {
                                error = EXIT_FAILURE;
                                goto error;
                            }
                            goto decode_tlv;
                            break;
                        case 0x03:
                            fprintf (message_stream, "NFC Forum application contains a \"NDEF Message TLV\".\n");
                            break;
                        case 0xFD:
                            fprintf (message_stream, "NFC Forum application contains a \"Proprietary TLV\", Skipping...\n");	// According to [ANNFC1K4K], we can skip this TLV to read further TLV blocks.
                            pbuffer += tlv_record_length(pbuffer, NULL, NULL);
                            if (pbuffer >= buffer + sizeof(buffer)) {
                                error = EXIT_FAILURE;
                                goto error;
                            }
                            goto decode_tlv;
                            break;
                        case 0xFE:
                            fprintf (stderr, "NFC Forum application contains a \"Terminator TLV\", no available data.\n");
                            error = EXIT_FAILURE;
                            goto error;
                            break;
                        default:
                            fprintf (stderr, "NFC Forum application contains an invalid TLV.\n");
                            error = EXIT_FAILURE;
                            goto error;
                            break;
                        }
                        if (fwrite (tlv_data, 1, tlv_data_len, ndef_stream) != tlv_data_len) {
                            fprintf (stderr, "Could not write to file.\n");
                            error = EXIT_FAILURE;
                            goto error;
                        }
                        free (tlv_data);
                    } else {
                        fprintf (stderr, "No NFC Forum application.\n");
                        error = EXIT_FAILURE;
                        goto error;
                    }
                } else {
                    fprintf (stderr, "No MAD detected.\n");
                    error = EXIT_FAILURE;
                    goto error;
                }
                free (mad);
            }

error:
            free (tag_uid);
        }
        fclose (ndef_stream);
		FreefareTag::free_tags (tags);
        nfc_close (device);
    }

    nfc_exit (context);
    return (error);
}

#ifndef MIFAREULTRALIGHTTEST_H
#define MIFAREULTRALIGHTTEST_H

#include "freefare_test.h"
#include "mifare_ultralight.h"

class MifareUltralight : public MifareUltralightTag, public FreefareTest
{
public:
	MifareUltralight(NfcDevice* dev, const NfcTarget& tgt);

	bool run();

private:
	static uint8_t new_key[];

	void dump_lock();
	bool test_read();
	bool test_write();
};

#endif // MIFAREULTRALIGHTTEST_H

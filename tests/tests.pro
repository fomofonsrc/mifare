INCLUDEPATH += ../freefare
TARGET = mifare
CONFIG -= qt

PRE_TARGETDEPS = ../freefare/libfreefare.a ../nfc/libnfc.a

INCLUDEPATH += ../include

LIBS += $$PRE_TARGETDEPS \
    -lcrypto -lusb

QMAKE_CFLAGS += -std=gnu99

SOURCES += \
    freefare_test.cpp \
    mifareclassictest.cpp \
    test.cpp \
    mifareultralightc.cpp \
    mifareultralight.cpp
#SOURCES += mifare-classic-read-ndef.cpp
#SOURCES += mifare-classic-write-ndef.cpp

HEADERS += \
    freefare_test.h \
    mifareclassictest.h \
    mifareultralightc.h \
    mifareultralight.h

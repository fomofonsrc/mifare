#include "mifareultralight.h"


MifareUltralight::MifareUltralight(NfcDevice* dev, const NfcTarget& tgt) :
	MifareUltralightTag(dev, tgt)
{

}

bool MifareUltralight::run()
{
	dump_lock();
//	test_write();
	test_read();
	return true;
}

void MifareUltralight::dump_lock()
{
	uint8_t d[16];
	if (read(0, d, sizeof(d)) > 0) {
		printf("OTP : %02x %02x %02x %02x\n", d[12], d[13], d[14], d[15]);
		printf("lock OTP: %u\n", (d[10] & 0x08) ? 1:0);
		printf("lock page  4: %u\n", (d[10] & 0x10) ? 1:0);
		printf("lock page  5: %u\n", (d[10] & 0x20) ? 1:0);
		printf("lock page  6: %u\n", (d[10] & 0x40) ? 1:0);
		printf("lock page  7: %u\n", (d[10] & 0x80) ? 1:0);
		printf("lock page  8: %u\n", (d[11] & 0x01) ? 1:0);
		printf("lock page  9: %u\n", (d[11] & 0x02) ? 1:0);
		printf("lock page 10: %u\n", (d[11] & 0x04) ? 1:0);
		printf("lock page 11: %u\n", (d[11] & 0x08) ? 1:0);
		printf("lock page 12: %u\n", (d[11] & 0x10) ? 1:0);
		printf("lock page 13: %u\n", (d[11] & 0x20) ? 1:0);
		printf("lock page 14: %u\n", (d[11] & 0x40) ? 1:0);
		printf("lock page 15: %u\n", (d[11] & 0x80) ? 1:0);
		return;
	}
	printf("read block %u error\n", 0);
}


bool MifareUltralight::test_read()
{
	bool ret = true;
	for (uint8_t i=16; i<16*4; i += 16) {
		uint8_t block[16];
		if (read(i, block, sizeof(block)) <= 0) {
			ret = false;
			printf("read block %u error\n", i/4);
		} else {
			dump_block(i/4, block);
		}
	}
	return ret;
}

#define PAGE_SIZE 4
#define TEST_PAGE 12

bool MifareUltralight::test_write()
{
	uint8_t dw[PAGE_SIZE] = { 0x30, 0, 0, 0 };
	if (write(TEST_PAGE * PAGE_SIZE, dw, PAGE_SIZE) < 0) {
		printf("%s() failed\n", __FUNCTION__);
		return false;
	}
	return true;
}

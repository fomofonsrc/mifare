#ifndef MIFARECLASSICTEST_H
#define MIFARECLASSICTEST_H

#include "mifare_classic.h"
#include "freefare_test.h"

class MifareClassic : public MifareClassicTag, public FreefareTest
{
public:
	MifareClassic(NfcDevice*, const NfcTarget&);

	bool run();

private:
	bool test_read();
	bool test_write();
	bool test_auth(const uint8_t* key);
};

#endif // MIFARECLASSICTEST_H

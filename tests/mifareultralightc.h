#ifndef MIFAREULTRALIGHTC_H
#define MIFAREULTRALIGHTC_H

#include "freefare_test.h"
#include "mifare_ulc.h"

class MifareUltralightC : public MifareUltralightCTag, public FreefareTest
{
public:
	MifareUltralightC(NfcDevice* dev, const NfcTarget& tgt);

	bool run();

private:
	static uint8_t old_key[];
	static uint8_t new_key[];

	bool test_read();
	bool test_write();
	bool test_auth(const uint8_t* key);
	bool test_change_key(const uint8_t* key);
	void dump_lock1();
	void dump_lock2();
};

#endif // MIFAREULTRALIGHTC_H

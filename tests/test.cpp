#include <err.h>
#include <stdlib.h>
#include <unistd.h>

#include "nfc.h"
#include "freefare_test.h"

int main (int /*argc*/, char ** /*argv*/)
{
//	while (true)
	{
		NfcContext *context;
		nfc_init (&context);
		if (context == NULL) {
			printf("Unable to alloc of NFC context\n");
		}

		nfc_connstring connstring[8];
		size_t device_count = nfc_list_devices (context, connstring, sizeof (connstring) / sizeof (*connstring));
		if (device_count <= 0) {
			printf("No NFC device found\n");
		}

		for (size_t d = 0; d < device_count; d++) {
			NfcDevice *device = nfc_open (context, connstring[d]);
			if (device) {
				FreefareTest::poll(device);
				nfc_close(device);
			} else {
				printf("nfc_open() failed\n");
			}
		}
		nfc_exit (context);
	}
	return 0;
}


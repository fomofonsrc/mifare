#include <cstdlib>
#include "mad.h"
#include "tlv.h"
#include "mifareclassictest.h"

MifareClassic::MifareClassic(NfcDevice* dev, const NfcTarget& tgt) :
	MifareClassicTag(dev, tgt)
{
}

bool MifareClassic::run()
{
	return test_read();
}

bool MifareClassic::test_read()
{
	MifareClassicTag* tag = static_cast<MifareClassicTag*>(this);
	Mad mad = mad_read(tag);
	if (mad == 0) {
		printf("No MAD detected.\n");
		return false;
	}

	// Dump the NFCForum application using MAD information
	uint8_t buffer[4096];
	const MadAid mad_nfcforum_aid = { 0xe1, 0x03 };
	ssize_t len = application_read (mad,
									mad_nfcforum_aid,
									buffer,
									sizeof(buffer),
									MifareClassicTag::nfcforum_public_key_a,
									MFC_KEY_A);

	if (len == -1) {
		printf ("No NFC Forum application.\n");
		free (mad);
		return false;
	}

	uint8_t tlv_type;
	uint16_t tlv_data_len;
	uint8_t * tlv_data;
	uint8_t * pbuffer = buffer;

decode_tlv:
	tlv_data = tlv_decode (pbuffer, &tlv_type, &tlv_data_len);
	switch (tlv_type) {
	case 0x00:
		printf("NFC Forum application contains a \"NULL TLV\", Skipping...\n");	// According to [ANNFC1K4K], we skip this Tag to read further TLV blocks.
		pbuffer += tlv_record_length(pbuffer, NULL, NULL);
		if (pbuffer >= buffer + sizeof(buffer)) {
			return false;
		}
		goto decode_tlv;
		break;

	case 0x03:
		printf ("NFC Forum application contains a \"NDEF Message TLV\".\n");
		break;

	case 0xFD:
		printf ("NFC Forum application contains a \"Proprietary TLV\", Skipping...\n");	// According to [ANNFC1K4K], we can skip this TLV to read further TLV blocks.
		pbuffer += tlv_record_length(pbuffer, NULL, NULL);
		if (pbuffer >= buffer + sizeof(buffer)) {
			return false;
		}
		goto decode_tlv;
		break;
	case 0xFE:
		printf ("NFC Forum application contains a \"Terminator TLV\", no available data.\n");
		return false;

	default:
		printf ("NFC Forum application contains an invalid TLV.\n");
		return false;
	}
#if 0
	if (fwrite (tlv_data, 1, tlv_data_len, ndef_stream) != tlv_data_len) {
		fprintf (stderr, "Could not write to file.\n");
		return false;
	}
#endif
	free (tlv_data);
	free (mad);
	return true;
}

bool MifareClassic::test_write()
{
	return true;
}

bool MifareClassic::test_auth(const uint8_t*)
{
	return true;
}


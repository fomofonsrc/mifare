#include "mifareultralightc.h"

#define PAGE_SIZE 4
#define TEST_PAGE 10
#define LOCK_PAGE2 40

uint8_t MifareUltralightC::new_key[16] = { 0x40, 0x45, 0x4D, 0x4B,
										  0x41, 0x45, 0x52, 0x42,
										  0x21, 0x4E, 0x41, 0x43,
										  0x55, 0x4F, 0x59, 0x46 };
uint8_t MifareUltralightC::old_key[16] = { 0x44, 0x45, 0x4D, 0x4B,
										  0x41, 0x45, 0x52, 0x42,
										  0x21, 0x4E, 0x41, 0x43,
										  0x55, 0x4F, 0x59, 0x46 };


MifareUltralightC::MifareUltralightC(NfcDevice* dev, const NfcTarget& tgt) :
	MifareUltralightCTag(dev, tgt)
{

}

bool MifareUltralightC::run()
{
	test_auth(default_key);
	dump_lock1();
	dump_lock2();
//	test_write();
	test_read();
//	test_change_key(new_key);
	return true;
}

bool MifareUltralightC::test_read()
{
	for (uint8_t i=16; i < LOCK_PAGE2 * PAGE_SIZE; i += 16) {
		uint8_t block[16];
		if (read(i, block, sizeof(block)) <= 0) {
			printf("read block %u error\n", i/PAGE_SIZE);
		} else {
			dump_block(i/4, block);
		}
	}
	return true;
}

bool MifareUltralightC::test_write()
{
	uint8_t dw[PAGE_SIZE] = { 0x30, 0, 0, 0 };
	if (write(TEST_PAGE * PAGE_SIZE, dw, PAGE_SIZE) < 0) {
		printf("%s() failed\n", __FUNCTION__);
		return false;
	}
	return true;
}

bool MifareUltralightC::test_auth(const uint8_t* key)
{
	bool ret = authenticate(key);
	if (ret) {
		printf ("Authentication success\n");
	} else {
		disconnect();
		connect();
		printf("Authentication failed\n");
	}
	return ret;
}

bool MifareUltralightC::test_change_key(const uint8_t* key)
{
	if (!changeKey(key)) {
		printf("changeKey() failed\n");
	}
//	disconnect();
//	connect();
	return true;
}

void MifareUltralightC::dump_lock1()
{
	uint8_t d[16];
	if (read(0, d, sizeof(d)) > 0) {
		printf("OTP : 0x%02x 0x%02x 0x%02x 0x%02x\n", d[12], d[13], d[14], d[15]);
		printf("lock OTP: %u\n", (d[10] & 0x08) ? 1:0);
		printf("lock page  4: %u\n", (d[10] & 0x10) ? 1:0);
		printf("lock page  5: %u\n", (d[10] & 0x20) ? 1:0);
		printf("lock page  6: %u\n", (d[10] & 0x40) ? 1:0);
		printf("lock page  7: %u\n", (d[10] & 0x80) ? 1:0);
		printf("lock page  8: %u\n", (d[11] & 0x01) ? 1:0);
		printf("lock page  9: %u\n", (d[11] & 0x02) ? 1:0);
		printf("lock page 10: %u\n", (d[11] & 0x04) ? 1:0);
		printf("lock page 11: %u\n", (d[11] & 0x08) ? 1:0);
		printf("lock page 12: %u\n", (d[11] & 0x10) ? 1:0);
		printf("lock page 13: %u\n", (d[11] & 0x20) ? 1:0);
		printf("lock page 14: %u\n", (d[11] & 0x40) ? 1:0);
		printf("lock page 15: %u\n", (d[11] & 0x80) ? 1:0);
		return;
	}
	printf("read block %u error\n", 0);
}

void MifareUltralightC::dump_lock2()
{
	uint8_t d[16];
	if (read(LOCK_PAGE2 * PAGE_SIZE, d, sizeof(d)) > 0) {
		printf("lock page 16-19: %u\n", (d[0] & 0x02) ? 1:0);
		printf("lock page 20-23: %u\n", (d[0] & 0x04) ? 1:0);
		printf("lock page 24-27: %u\n", (d[0] & 0x08) ? 1:0);
		printf("lock page 28-31: %u\n", (d[0] & 0x20) ? 1:0);
		printf("lock page 32-35: %u\n", (d[0] & 0x40) ? 1:0);
		printf("lock page 36-39: %u\n", (d[0] & 0x80) ? 1:0);
		printf("lock Counter: %u\n", (d[1] & 0x10) ? 1:0);
		printf("lock AUTH0: %u\n", (d[1] & 0x20) ? 1:0);
		printf("lock AUTH1: %u\n", (d[1] & 0x40) ? 1:0);
		printf("lock Key: %u\n", (d[1] & 0x80) ? 1:0);
		printf("Counter: 0x%02x%02x\n", d[4], d[5]);
		printf("AUTH0: 0x%02x\n", d[8]);
		printf("AUTH1: 0x%02x\n", d[12]);
	}
}
